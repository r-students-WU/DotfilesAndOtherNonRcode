# Requirements

Tested with texlive-2017 (in Ubuntu 18.04 repos).
Requires ```texlive-xetex``` and ```biber``` packages. 
This should be installed if you did

```
sudo apt install texlive-full
```
To recompile you should only keep

"knit.R"
"compile.sh"
"rAndLateX.Rnw"

First open and run "knit.R". This will create a ".tex" file which can den be compiled to pdf using the "compile.sh" file. Run

```
./compile.sh
```

in the command line. If you get a permission denied error try

```
chmod +x compile.sh
```

and rerun. 
