Installs required packages automatically. Preferred is always the stable version of a package but latest will be installed if its the only available version. This setup works with LaTeX, Rmarkdown, R, Python. It has autocompletion and uses evil mode (vim keybindings). Tested on Ubuntu 17.10 and 18.04. I don't know if it works on any other system. In any case you have to change the path to your setup if you'd like to reuse it. For info about emacs visit:

https://www.gnu.org/software/emacs/
