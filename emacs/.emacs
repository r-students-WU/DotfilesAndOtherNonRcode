;;; Initial configs
;; Set window title to current Dir
((setq frame-title-format
          '(buffer-file-name "%f"
            (dired-directory dired-directory "%b")))
;; Activate autocompletion
(add-hook 'after-init-hook 'global-company-mode)
;; Do not show welcome screen
(setq inhibit-splash-screen t)
;; Some default views
(transient-mark-mode 1)
(show-paren-mode 1)
(setq show-paren-delay 0)
(electric-pair-mode 1)
;; Backup
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq delete-old-versions t)
(setq kept-new-versions 10)
(setq kept-old-versions 1)
(setq vc-make-backup-files t)
(setq version-control t)
(setq vc-make-backup-files t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))
;; Start in dired 
(dired ".")
; Keyboard Shortcuts (non-package)
(global-set-key (kbd "S-s") 'shell)
; Packages
;; Init
(require 'package)
(setq package-archives
      '(("GNU ELPA"     . "http://elpa.gnu.org/packages/")
        ("MELPA Stable" . "https://stable.melpa.org/packages/")
        ("MELPA"        . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("MELPA Stable" . 10)
        ("GNU ELPA"     . 5)
        ("MELPA"        . 0)))
(package-initialize)
;; Get use-package
(if (not (package-installed-p 'use-package))
    (progn
      (package-refresh-contents)
      (package-install 'use-package)))
(require 'use-package)
;; Load my packages and configs
;;; LaTeX
(use-package tex
  :ensure auctex
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-engine 'xetex)
  :init
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
  (add-hook 'LaTeX-mode-hook 'visual-line-mode)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode)
  (add-hook 'LaTeX-mode-hook
	    (lambda()
	      (add-to-list (make-local-variable 'company-backends)
			   'company-math-symbols-latex
			   'company-latex-commands)))
)

(use-package cdlatex
  :ensure t)

(use-package reftex
  :commands turn-on-reftex
  :config (setq reftex-plug-into-AUCTeX t))

;;; Git
(use-package magit
  :ensure t
  :config (global-set-key (kbd "C-x g") 'magit-status))

;;; Org
(use-package org
  :ensure t
  :config
  (global-set-key "\C-cl" 'org-store-link)
  (global-set-key "\C-ca" 'org-agenda)
  (global-set-key "\C-cc" 'org-capture)
  (global-set-key "\C-cb" 'org-iswitchb)
  )

;;; Helm
(use-package helm
  :ensure t
  :config
  (require 'helm-config)
  (helm-mode 1)
  :bind (("M-x" . helm-M-x)
         ("C-x C-f" . helm-find-files)
         ([f10] . helm-buffers-list)
         ([S-f10] . helm-recentf)
	 ("C-x C-b" . helm-buffers-list)))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

;;; Programing Languages
(use-package python
  :ensure t
  :mode ("\\.py\\'" . python-mode)
  :config
 ;; (defvaralias 'flycheck-python-flake8-executable 'python-shell-interpreter)
  (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode)
  (defun my/python-mode-hook ()
    (add-to-list 'company-backends 'company-jedi)
    (add-to-list 'company-backends 'company-anaconda))
  (add-hook 'python-mode-hook 'my/python-mode-hook)
)

(use-package anaconda-mode
  :ensure t
  :config
   (eval-after-load 'company
    '(add-to-list 'company-backends '(company-anaconda :with company-capf))))

(use-package ess
  :ensure t
  :commands R
  :init
  (require 'ess-site)
  :config
  (setq ess-smart-S-assign-key ";")
  (ess-toggle-S-assign nil)
  (ess-toggle-S-assign nil)
  (ess-toggle-underscore nil) ; leave underscore key alone!
  (setq ess-fancy-comments nil)
  ; Make ESS use RStudio's indenting style
  (add-hook 'ess-mode-hook (lambda() (ess-set-style 'RStudio)))
  ; Make ESS use more horizontal screen
  (add-hook 'ess-R-post-run-hook 'ess-execute-screen-options)
  (define-key inferior-ess-mode-map "\C-cw" 'ess-execute-screen-options)
  (defun my/ess-mode-hook ()
    (add-to-list 'company-backends 'company-R-args)
    (add-to-list 'company-backends 'company-R-objects))
  (add-hook 'ess-mode-hook 'my/ess-mode-hook)
  (setq ess-use-company nil)
  (add-to-list 'auto-mode-alist '("\\.Rnw\\'" . Rnw-mode))
  (add-to-list 'auto-mode-alist '("\\.Snw\\'" . Rnw-mode))
  (add-to-list 'auto-mode-alist '("\\.Rmd\\'" . Rnw-mode))

  ;; Make TeX and RefTex aware of Snw and Rnw files
  (setq reftex-file-extensions
      '(("Snw" "Rnw" "nw" "tex" ".tex" ".ltx") ("bib" ".bib")))
  (setq TeX-file-extensions
      '("Snw" "Rnw" "nw" "tex" "sty" "cls" "ltx" "texi" "texinfo"))

  ;; Lets you do 'C-c C-c Sweave' from your Rnw file
  (add-hook 'Rnw-mode-hook
	  (lambda ()
	    (add-to-list 'TeX-command-list
			 '("Sweave" "R CMD Sweave %s"
			   TeX-run-command nil (latex-mode) :help "Run Sweave") t)
	    (add-to-list 'TeX-command-list
			 '("LatexSweave" "%l %(mode) %s"
			   TeX-run-TeX nil (latex-mode) :help "Run Latex after Sweave") t)
	    (setq TeX-command-default "Sweave")))
  (setq ess-swv-processor "'knitr")
  (setq ess-ask-for-ess-directory nil)
  (setq ess-local-process-name "R")
  (setq ansi-color-for-comint-mode 'filter)
  (setq comint-scroll-to-bottom-on-input t)
  (setq comint-scroll-to-bottom-on-output t)
  (setq comint-move-point-for-output t)
  (defun my-ess-start-R ()
    (interactive)
    (if (not (member "*R*" (mapcar (function buffer-name) (buffer-list))))
      (progn
	(delete-other-windows)
	(setq w1 (selected-window))
	(setq w1name (buffer-name))
	(setq w2 (split-window w1 nil t))
	(R)
	(set-window-buffer w2 "*R*")
	(set-window-buffer w1 w1name))))
  (defun my-ess-eval ()
    (interactive)
    (my-ess-start-R)
    (if (and transient-mark-mode mark-active)
	(call-interactively 'ess-eval-region)
      (call-interactively 'ess-eval-line-and-step)))
  (add-hook 'ess-mode-hook
	    '(lambda()
	       (local-set-key [(shift return)] 'my-ess-eval)))
  (add-hook 'inferior-ess-mode-hook
	    '(lambda()
	       (local-set-key [C-up] 'comint-previous-input)
	       (local-set-key [C-down] 'comint-next-input)))
 (add-hook 'Rnw-mode-hook 
          '(lambda() 
             (local-set-key [(shift return)] 'my-ess-eval))) 
)


(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init
  (setq markdown-command "pandoc")
  (setq markdown-enable-math t)
  (add-hook 'markdown-mode-hook 'flyspell-mode)
  (add-hook 'markdown-mode-hook 'visual-line-mode)
  (add-hook 'markdown-mode-hook 'turn-on-cdlatex)
  )

(use-package pandoc-mode
  :ensure t
  :config
  (add-hook 'markdown-mode-hook 'pandoc-mode)
  (add-hook 'pandoc-mode-hook 'pandoc-load-default-settings))

(add-hook 'text-mode-hook (lambda() (flyspell-mode 1)))


(use-package polymode
  :ensure t
  :config
  (require 'poly-R)
  (require 'poly-markdown)
  (add-to-list 'auto-mode-alist '("\\.Rmd" . poly-markdown+r-mode))
  (add-to-list 'auto-mode-alist '("\\.Snw" . poly-noweb+r-mode))
  (add-to-list 'auto-mode-alist '("\\.Rnw" . poly-noweb+r-mode)))

;;; Yasnippet
(use-package yasnippet
  :ensure t
  :config
  (require 'yasnippet)
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :ensure t)

;;; Evil
(use-package evil
  :ensure t
  :config
  (require 'evil)
  (evil-mode 1)
  (define-key evil-normal-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
  (define-key evil-normal-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
  (define-key evil-motion-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
  (define-key evil-motion-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
;;; Make horizontal movement cross lines
  (setq-default evil-cross-lines t))
	    
(use-package neotree
  :ensure t
  :config
  (global-set-key [f8] 'neotree-toggle))

;;; Company (Autocompletions)

(use-package company
  :defer 1
  :ensure t
  :diminish company-mode
  :init (require 'company-math)
  :config
  (setq company-backends
	'((company-files          ; files & directory
         company-keywords       ; keywords
         company-capf
         ;company-yasnippet
         )
        (company-abbrev company-dabbrev)
        ))
   ;(setq ess-use-company t)
  (setq company-dabbrev-downcase 0)
  (setq company-idle-delay 0.1)
  (setq company-minimum-prefix-length 1)
  (add-to-list 'company-backends 'company-math-symbols-unicode)
  ;; (eval-after-load 'company
  ;;   '(add-to-list 'company-backends 'company-irony))
  ;; (eval-after-load 'company
  ;;   '(add-to-list 'company-backends '(company-anaconda :with company-capf)))
  (global-company-mode))

(use-package company-auctex
  :ensure t
  :config (company-auctex-init))

(use-package company-math
  :ensure t
  :config
  (setq company-math-allow-latex-symbols-in-faces t))

(use-package company-jedi
  :ensure t)

(use-package company-irony
  :ensure t
  )

(use-package company-anaconda
  :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-view-program-selection
   (quote
    (((output-dvi has-no-display-manager)
      "dvi2tty")
     ((output-dvi style-pstricks)
      "dvips and gv")
     (output-dvi "xdvi")
     (output-pdf "Atril")
     (output-html "xdg-open"))))
 '(company-dabbrev-minimum-length 3)
 '(company-show-numbers t)
 '(company-tooltip-limit 15)
 '(custom-enabled-themes (quote (adwaita)))
 '(flycheck-python-pycompile-executable "/home/daniel/anaconda3/bin/python")
 '(package-selected-packages (quote (magit auctex use-package dash)))
 '(python-shell-interpreter "/home/daniel/anaconda3/bin/python")
 '(show-paren-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#f2f1f0" :foreground "#4c4c4c" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 125 :width normal :foundry "simp" :family "Hack")))))
